#include <opencv2/opencv.hpp>
#include <chrono>
#include <stdio.h>
#include "db.h"

cv::Mat img;
bool isOn;
DB db;

int main(int argc, char *argv[]) {
  cv::VideoCapture cap(argv[1]);

  int x,y,h,w, minHue, maxHue, alrm;
  CvFileStorage* fs = cvOpenFileStorage("config.xml", 0, CV_STORAGE_READ);
  x = cvReadIntByName(fs, 0, "x", 0);
  y = cvReadIntByName(fs, 0, "y", 0);
  w = cvReadIntByName(fs, 0, "w", 60);
  h = cvReadIntByName(fs, 0, "h", 60);
  minHue = cvReadIntByName(fs, 0, "minHue", 60);
  maxHue = cvReadIntByName(fs, 0, "maxHue", 60);
  alrm = cvReadIntByName(fs, 0, "alarmSeconds", 60);

  cv::Rect box(x,y,w,h);
  cv::Size blurSize(w, h);

  db.init("192.168.100.50", "root", "bersaudara", "bpbd_jkt");

  bool internalOn = false;

  auto timeOn = std::chrono::system_clock::now();

  int frameCount = 0;



  while (cv::waitKey(1) != 27) {
    cap >> img;
    cv::Mat crop(img, box);
    cv::blur(crop, crop, blurSize);

    cv::Mat crophsv;
    cv::cvtColor(crop, crophsv, cv::COLOR_RGB2HSV);
    cv::Vec3b p = crophsv.at<cv::Vec3b>(w/2,h/2);
    int hue = (int) p[0];

    if (hue < minHue || hue > maxHue) {
      internalOn = true;
    }
    
    if (internalOn && !isOn) {
      timeOn = std::chrono::system_clock::now();
      isOn = true;
    }

    if (!internalOn) {
      isOn = false;
    }

    if (isOn) {
      auto now = std::chrono::system_clock::now();
      int duration = std::chrono::duration_cast<std::chrono::seconds>(now - timeOn).count();
      if (duration > alrm) {
        char filename[128];
        sprintf(filename, "img/banjir-%d.jpg", frameCount);
        cv::imwrite(filename, img);
        db.insert(std::string("BANJIR"), std::string(filename));
        timeOn = std::chrono::system_clock::now();
      }
    }

    // cv::imshow("w", crop);
    // cv::imshow("w", img);
    frameCount++;
  }

  cv::imwrite("img.jpg", img);
}
